const checkNumber = document.querySelector(".checkNumber"); //находим DIV с инпутами и создаем поле ввода номера
const inputField = document.createElement("input");
inputField.setAttribute("placeholder", "000-000-00-00");
checkNumber.append(inputField);

const button = document.createElement("input"); //создаем кнопку отправки номера
button.setAttribute("type", "submit");
button.setAttribute("value", "Сохранить");
checkNumber.append(button);

const pattern = /^\d\d\d-\d\d\d-\d\d-\d\d$/;    //создаю регулярку

button.addEventListener("click", func);
function func() {


    if (pattern.exec(inputField.value)) {           //если номер соответствует шаблону
        inputField.style.backgroundColor = "green";
        document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"
    } else {
        const errorText = document.createElement("p");    //если не соответствует
        errorText.innerHTML = "Вы ошиблись. Обновите страницу и попробуйте еще раз";
        checkNumber.prepend(errorText);

    }
    button.removeEventListener("click", func);

}
